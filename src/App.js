import React from 'react';
import Listproduct from './List';
import Navegador from './header';

function App() {
  return (
    <div className="App">
      <Navegador/>
      <Listproduct/>
    </div>
  );
}

export default App;

import React, { Component } from 'react';
import {Form,Button} from 'react-bootstrap';

export default class AddProduct extends Component {
  state={
    foto:'',
    titulo: '',
    preco: ''

  }
  handleChangeF= (e) =>{
    this.setState({
      foto: e.target.value
    })
    console.log(this.state)
  }
  handleChangeT= (e) =>{
    this.setState({
      titulo: e.target.value
    })
    console.log(this.state)
  }
  handleChangeP= (e) =>{
    this.setState({
      preco: e.target.value
    })
    console.log(this.state)
  }
  hendleSubmit = (e)=>{
    e.preventDefault();
    this.props.addprodutucs(this.state)
    this.setState({
      foto:'',
      titulo: '',
      preco: '',
    })
  }
  render() {
    return (
    <div className="Formadd">
     
      <Form onSubmit={this.hendleSubmit}>
        <Form.Group >
          <Form.Label>foto</Form.Label>
          <Form.Control type="text" placeholder="Adicione nome da foto" onChange={this.handleChangeF} value={this.state.foto} />
        </Form.Group>

        <Form.Group >
          <Form.Label>Produto</Form.Label>
          <Form.Control type="text" placeholder="Adicione um produto" onChange={this.handleChangeT} value={this.state.titulo} />
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>

        <Form.Group >
          <Form.Label>Preco</Form.Label>
          <Form.Control type="Text" placeholder="Adicione preco do produto"  onChange={this.handleChangeP} value={this.state.preco} />
        </Form.Group>
        <Button type="submit">
        enviar
        </Button>
      </Form>
    </div>
      );
  }
}

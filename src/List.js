import React, { Component } from 'react';
import Products from './Products';
import AddProduct from './AddProduct';
import {Row, Container, Col} from 'react-bootstrap';
import './List.css';

export default class Listproduct extends Component {
  state={
    produtos:[
      {id:'1', titulo:'Coca Cola', preco:'30'},
      {id:'2', titulo:'Guarana', preco:'30'}
    ]
  }
  deleteProdutc = (id) => {
    const produtos = this.state.produtos.filter(produto =>{
      return produto.id !== id
    });
    this.setState({produtos});
  }
  addprodutucs = (produto) =>{
    produto.id = Math.random();
    let produtos =[...this.state.produtos, produto];
    this.setState({
      produtos: produtos
    })
  }
  render() {
    return (
      <div>
        <Container>
          <Row>
            <Col xs={4}>
        <AddProduct addprodutucs={this.addprodutucs}/>  
        </Col>
        <Col xs={8}>     
        <Products produtos={this.state.produtos} deleteProdutc={this.deleteProdutc}/>
        </Col>
        </Row>
        </Container>
      </div>
    );
  }
}
